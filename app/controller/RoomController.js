'use strict';
const RoomsModel = require('../models/RoomsModel')
const fs = require('fs');

class RoomController {

    async getAllRooms(req, res) {
        const data = await RoomsModel.find();
        try {
            const data_res = {
                status: 200,
                message: "Data successfully retried",
                data: data

            }
            res.send(data_res)
        } catch (e) {
            const error_res = {
                status: 500,
                message: "Failed to retrieve data"
            }
            res.send(error_res)

        }


    }

    async getRoomById(req, res) {
        const data = await RoomsModel.findById(req.params.userId)

        try {
            const data_res = {
                status: 200,
                message: "Data successfully retried",
                data: data
            }
            res.send(data_res)
        } catch (e) {
            const error_res = {
                status: 500,
                message: "Failed to retrieve data"
            }
            res.send(error_res)
        }
    }
   
    async createRoom(req, res) {


        let buff = new Buffer(req.body.image, 'base64');
        let imageName = 'storage/'+Math.floor(Date.now() / 1000)+'.jpg'
        fs.writeFileSync(imageName, buff);

        const data = new RoomsModel({
            title: req.body.title,
            description: req.body.description,
            price: req.body.price,
            location: req.body.location,
            owner: req.body.owner,
            image: imageName,
        });


        try {
            const saveData = await data.save();
            const data_res = {
                status: 200,
                message: "User succesafully registered",
                data: saveData
            }
            res.send(data_res)

        } catch (err) {
            const error_res = {
                status: 500,
                message: "Failed to save data" + err
            }
            res.send(error_res)
        }

    }



}

module.exports = RoomController