const UserModel = require('../models/UserModel')

class UserController {

    async getUser(req, res) {
        const data = await UserModel.find();
        try {
            const data_res = {
                status: 200,
                message: "Data successfully retried",
                data: data

            }
            res.send(data_res)
        } catch (e) {
            const error_res = {
                status: 500,
                message: "Failed to retrieve data"
            }
            res.send(error_res)

        }


    }

    async getUserById(req, res) {
        const data = await UserModel.findById(req.params.userId)

        try {
            const data_res = {
                status: 200,
                message: "Data successfully retried",
                data: data
            }
            res.send(data_res)
        } catch (e) {
            const error_res = {
                status: 500,
                message: "Failed to retrieve data"
            }
            res.send(error_res)
        }
    }
    async getUserByEmail(email) {

        try {
            const data = await UserModel.find({ email: email })
            console.log(data)
            return data.length===0
        } catch (e) {
            console.log(e)
            return null
        }
    }

    async createUser(req, res) {

        console.log(req.body.name)
        const data = new UserModel({
            email: req.body.email,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            cellNumber: req.body.cellNumber,
            gender: req.body.gender,
            userRole: req.body.userRole,
            password: req.body.password
        });


        try {
            const saveData = await data.save();
            const data_res = {
                status: 200,
                message: "User succesafully registered",
                data: saveData
            }
            res.send(data_res)

        } catch (err) {
            const error_res = {
                status: 500,
                message: "Failed to save data" + err
            }
            res.send(error_res)
        }

    }

    async signUp(req, res) {

        const {email} = req.body

        const checkEmail = await this.getUserByEmail(email)

        if(checkEmail===true){
            await this.createUser(req, res)
        }else if(checkEmail===false){
            const error_res = {
                status: 500,
                message: "Email already exist"
            }
            res.send(error_res)
        }else{
            const error_res = {
                status: 500,
                message: "Something went wrong, please contact your provider"
            }
            res.send(error_res)
        }
        
    }
    async signIn(req, res) {

        const {email, password} = req.body

        const data = await UserModel.find({email: email, password: password});

        try {
            const data_res = {
                status: 200,
                message: "Data successfully retried",
                data: data

            }
            res.send(data_res)
        } catch (e) {
            const error_res = {
                status: 500,
                message: "Failed to retrieve data"
            }
            res.send(error_res)

        }
        
    }
    async deleteUserById(req, res) {
        try {
            const data = await UserModel.remove({ _id: req.params.userId });

            const data_res = {
                status: 200,
                message: "Data successfully deleted",
                data: data
            }
            res.send(data_res)
        } catch (err) {
            const error_res = {
                status: 500,
                message: "Failed to delete data"
            }
            res.send(error_res)
        }
    }

    async updateUser(req, res) {
        const userId = req.params.userId;

        const body = req.body;

        try {
            const data = await UserModel.updateOne({ _id: userId }, {
                $set: {
                    email: req.body.email,
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    cellNumber: req.body.cellNumber,
                    gender: req.body.gender,
                    userRole: req.body.userRole
                }
            });
            const data_res = {
                status: 200,
                message: "Data successfully updated",
                data: data
            }
            res.send(data_res)
        } catch (err) {
            const error_res = {
                status: 500,
                message: "Failed to update requirements" + err
            }
            res.send(error_res)
        }

    }

}

module.exports = UserController