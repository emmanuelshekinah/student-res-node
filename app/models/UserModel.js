const mongoose = require('mongoose')

const UserSchema = mongoose.Schema({
    email:{
        type: String,
         required: true
    }, 
    password: {
        type: String,
        
    },
    firstName:{
        type: String,
        // required: true
    }, 
    lastName:{
        type: String,
        // required: true
    },
    cellNumber:{
        type: String,
        // required: true
    },
    gender:{
        type: String,
        // required: true
    },
    userRole:{
        type: Number,
        // required: true
    },
    
})

module.exports = mongoose.model('user', UserSchema)