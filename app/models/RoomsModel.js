const mongoose = require('mongoose')

const RoomSchema = mongoose.Schema({
    title:{
        type: String,
        //  required: true
    }, 
    description: {
        type: String,
        
    },
    price:{
        type: String,
        // required: true
    }, 
    location:{
        type: String,
        // required: true
    },
    owner:{
        type: String,
        // required: true
    },
    image: {
        type: String
    }
    
})

module.exports = mongoose.model('rooms', RoomSchema)