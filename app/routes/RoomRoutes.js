const express = require("express");
const res = require("express/lib/response");
const router = express.Router();
const RoomController = require('../controller/RoomController')

const roomController = new RoomController()

router.get('/',function(req, res){roomController.getAllRooms(req,res)})
router.post('/', function (req, res) {roomController.createRoom(req, res)})


module.exports = router;