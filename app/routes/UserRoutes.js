const express = require("express");
const res = require("express/lib/response");
const router = express.Router();
const UserController = require('../controller/UserController')

const userController = new UserController()

router.get('/',function(req, res){userController.getUser(req,res)})
router.get('/:userId', function(req, res){userController.getUserById(req, res)})
router.post('/', function (req, res) {userController.createUser(req, res)})
router.post('/signup', function (req, res) {userController.signUp(req, res)})
router.post('/signin', function (req, res) {userController.signIn(req, res)})
router.delete('/:userId', function (req, res) { userController.deleteUserById(req, res)})
router.patch('/:userId', function (req, res) {userController.updateUser(req, res);})


module.exports = router;