const express = require('express');
const bodyParser = require('body-parser');
const UserRoutes = require('./routes/UserRoutes')
const RoomRoutes = require('./routes/RoomRoutes')
const mongose = require('mongoose');
const cors = require('cors')
require('dotenv').config()
var app = express();

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true,limit: '50mb' }));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.raw({limit: '50mb'}));


app.use('/user', UserRoutes)
app.use('/room', RoomRoutes)
app.use('/storage', express.static('storage'));

mongose.connect(process.env.DB_CONNECTION, {useNewUrlParser: true}).then((res)=>{
   console.log("DB Connected.......!")

}).catch(err=>{
   console.log("Failed to connect to mongo DB ", err)
})
// .catch((err)=>{console.log("Failed",err)})
const port = process.env.PORT === undefined ? 5300 : process.env.PORT
var server = app.listen(port, function () {  
   var host = server.address().address
  
   
   console.log("App Running on port ", port)
})

///Test
